FROM python:3.10.5-slim

ENV DEBIAN_FRONTEND=noninteractive

WORKDIR /app

COPY . .

RUN pip3 install -r requirements.txt

CMD ["python", "bot.py"]