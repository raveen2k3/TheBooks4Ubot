import io
from pyrogram import *
from pyrogram.types import *

import pymongo
from libgen_api import LibgenSearch
from pyrogram.enums import chat_type, ParseMode

from config import API_HASH, API_ID, BOT_TOKEN , DB_NAME , DB_URL, OWNER

import asyncio


Rias = Client(name="bot" , api_id= API_ID ,api_hash= API_HASH ,bot_token= BOT_TOKEN )


prefixes=['!' , '/']

BOT_VERSION = "1.0"


c = pymongo.MongoClient(DB_URL)
col = c["Books4U"]["Raveen"]

'''def addusertoDb(message):
    my_client = pymongo.MongoClient(DB_URL)
    
    Database = my_client[DB_NAME]
    userid=message.from_user.id
    chat_type = message.chat.type
    collection = Database["users"]
    result = collection.find_one({'userid': userid})

    if result:
        if result.get('userid'): return
        
    username = message.from_user.username
    firstname = message.from_user.first_name
    lastname = message.from_user.last_name
    
    user = {}
    user['userid'] = userid
    user['chattype'] = str(chat_type)
    user['username'] = username
    user['firstname'] = firstname
    user['lastname'] = lastname

    collection.insert_one(user)'''


async def add_users(id):
  dic = {"_id": "bcast", "data": {"all_users": [id], "users": [id]}}
  r = col.find_one({"_id": "bcast"})
  if r:
    us=get_users()["users"]
    if id in us:
      pass
    else:
      col.update_one({"_id": "bcast"}, {"$push": {"data.users": id}})
  else:
    col.insert_one(dic)

async def remove_users(id):
    col.update_one({"_id": "bcast"}, {"$pull": {"data.users": id}})


def get_users():
    r = col.find_one({"_id": "bcast"})
    if r:
        return r["data"]
    else:
        return {}

async def add_all(id):
    lis = get_users()
    lis = lis["all_users"]
    if id in lis:
        pass
    else:
        col.update_one({"_id": "bcast"}, {"$push": {"data.all_users": id}})




@Rias.on_message(filters.command(["start"] , prefixes) & filters.private)
async def start(client, message): 
  
    await add_users(message.from_user.id)
    await add_all(message.from_user.id)

    await message.reply_text(
        text=f"**Hello {message.from_user.first_name} 👋 !"
            "\n\nFeeling Tired of searching your favourite books , we are here to help you. "
            "\n\nCheck About to know the use of me**",
        disable_web_page_preview=True,
        reply_markup=InlineKeyboardMarkup(
            [
                [
                    InlineKeyboardButton("About", callback_data="About"),
                ]
            ]
        ),
        reply_to_message_id=message.id
    )
    addusertoDb(message)


MAX_MESSAGE_LENGTH = 4096

ownerId = int(1871813121)


    
@Rias.on_message(filters.command(["search"] , prefixes))
async def search(client , message):

  await add_users(message.from_user.id)
  await add_all(message.from_user.id)

  m=message.command
  if len(m)==1:
    return await message.reply("❌ **INVALID COMMAND**\n\n🔰Enter book name also in commoand")

  t=await message.reply("`Searching this book...`")
  name=message.from_user.first_name
  bookname=" ".join(m[1:])
  global s
  s = LibgenSearch()
  global results
  results = s.search_title(bookname)
  global l
  l=len(results)
  if l==0:
    return await t.edit_text("❌ **BOOK NOT FOUND**")
  #print(bookname)
  global i
  i = 0
  item_to_download = results[i]
  download_links = s.resolve_download_links(item_to_download)
    
  data_text = "✅ **Title:** `" + results[i]['Title'] +"`\n\n✍️ **Author:** " + results[i]['Author'] + "\n**🎙️ Language:** " + results[i]['Language'] + "\n🔰 **published on:** " + results[i]['Year'] + "\n🔰 **Size:** " + results[i]['Size'] + "\n📑 **Ext:** " + results[i]['Extension']
  data_text=data_text+f"\n❄️ **Requested By:**  [{name}](tg://user?id={str(message.from_user.id)})"
  item_to_download = results[i]
  download_links = s.resolve_download_links(item_to_download)

  if l==1:
    await message.reply(data_text , reply_markup=InlineKeyboardMarkup(
            [
                [InlineKeyboardButton(text="Link1" , url=download_links["GET"]) ,
                 InlineKeyboardButton(text="❌", callback_data=f"close|{message.from_user.id}"),
                 InlineKeyboardButton(text="Link2" , url=download_links["Cloudflare"])]
            ]
        )
    )   
  else:  
    await message.reply(data_text , reply_markup=InlineKeyboardMarkup(
            [
                [InlineKeyboardButton(text="Link1" , url=download_links["GET"]) ,
                 InlineKeyboardButton(text="Link2" , url=download_links["Cloudflare"])],
                [InlineKeyboardButton("Next⏩", callback_data=f"next1|{message.from_user.id}|{name}"),
                 InlineKeyboardButton(text="Close ❌", callback_data=f"close|{message.from_user.id}")]
            ]
        )
    )   
  await t.delete() 






@Rias.on_message(filters.command(["brodcast"] , prefixes))
async def brodcast (client , message):
    user_id = message.from_user.id
    if user_id == ownerId:
        o = []

        try:
                
            my_client = pymongo.MongoClient(DB_URL)
        
            Database = my_client[DB_NAME]
            userid=message.from_user.id
            collection = Database["users"]
            receivers = [c["userid"] for c in collection.find()]
            for receiver in receivers:
                try:
                    message_to_send = message.reply_to_message.id
                    print(receiver)
                    
                    await Rias.forward_messages(chat_id=receiver, from_chat_id=message.chat.id , message_ids=message_to_send)
                except:
                    o.append(receiver)
                    await Rias.send_message(chat_id=ownerId, text="brodcast failed to " + str(o))
                    continue
                
                
        except Exception as e:
            
            error_msg = str(e)
            await Rias.send_message(chat_id=ownerId, text=error_msg)



        
@Rias.on_callback_query(filters.regex("next"))
async def next_things(Rias, query):
      d=query.data.split("|")
      uid=d[1]
      nem=d[2]
      num=int(d[0].replace("next", ""))
      if query.from_user.id != int(uid):
        #if query.from_user.id not in OWNER+db.get_admin():
          try:
            return await query.answer(
              "❌ This buttoms is not for you!\nSearch your own book.", show_alert=True
              )
          except:
            return 

      if l-1==num:
        await query.answer()
        data_text = "✅ **Title:** `" + results[i+num]['Title'] +"`\n\n✍️ **Author:** " + results[i+num]['Author'] + "\n**🎙️ Language:** " + results[i+num]['Language'] + "\n🔰 **published on:** " + results[i+num]['Year'] + "\n🔰 **Size:** " + results[i+num]['Size'] + "\n📑 **Ext:** " + results[i+num]['Extension']
        data_text=data_text+f"\n❄️ **Requested By:**  [{nem}](tg://user?id={uid})"
        item_to_download = results[i+num]
        download_links = s.resolve_download_links(item_to_download)
        await query.message.edit_text(text=data_text, reply_markup =InlineKeyboardMarkup([
            [InlineKeyboardButton(text="Link1" , url=download_links["GET"]) ,
             InlineKeyboardButton(text="Link2" , url=download_links["Cloudflare"])],
            [InlineKeyboardButton("Back⏪" , callback_data=f"next{str(num-1)}|{uid}|{nem}"),
             InlineKeyboardButton(text="Close ❌", callback_data=f"close|{uid}")]]
                                                      ))
    
      elif num==0:
        await query.answer()
        data_text = "✅ **Title:** `" + results[0]['Title'] +"`\n\n✍️ **Author:** " + results[0]['Author'] + "\n**🎙️ Language:** " + results[0]['Language'] + "\n🔰 **published on:** " + results[0]['Year'] + "\n🔰 **Size:** " + results[0]['Size'] + "\n📑 **Ext:** " + results[0]['Extension']
        data_text=data_text+f"\n❄️ **Requested By:**  [{nem}](tg://user?id={uid})"
        item_to_download = results[0]
        download_links = s.resolve_download_links(item_to_download)
        await query.message.edit_text(text= data_text, reply_markup=InlineKeyboardMarkup(
            [
                [InlineKeyboardButton(text="Link1" , url=download_links["GET"]) ,
                 InlineKeyboardButton(text="Link2" , url=download_links["Cloudflare"])],
                [InlineKeyboardButton("Next⏩", callback_data=f"next1|{uid}|{nem}"),
                 InlineKeyboardButton(text="Close ❌", callback_data=f"close|{uid}")]]
                                ))
        
      else:
        await query.answer()
        data_text = "✅ **Title:** `" + results[i+num]['Title'] +"`\n\n✍️ **Author:** " + results[i+num]['Author'] + "\n**🎙️ Language:** " + results[i+num]['Language'] + "\n🔰 **published on:** " + results[i+num]['Year'] + "\n🔰 **Size:** " + results[i+num]['Size'] + "\n📑 **Ext:** " + results[i+num]['Extension']
        data_text=data_text+f"\n❄️ **Requested By:**  [{nem}](tg://user?id={uid})"
        item_to_download = results[i+num]
        download_links = s.resolve_download_links(item_to_download)
        await query.message.edit_text(text=data_text, reply_markup =InlineKeyboardMarkup([
            [InlineKeyboardButton(text="Link1" , url=download_links["GET"]) ,
             InlineKeyboardButton(text="Link2" , url=download_links["Cloudflare"])],
            [InlineKeyboardButton("⏪" , callback_data=f"next{str(num-1)}|{uid}|{nem}"),
             InlineKeyboardButton(text="❌", callback_data=f"close|{uid}"),
             InlineKeyboardButton("⏩" , callback_data=f"next{str(num+1)}|{uid}|{nem}")]]
                                                      ))
 
    


@Rias.on_callback_query(filters.regex("close"))
async def close_buttom(Rias, query):
    d=query.data.split("|")
    uid=d[1]
    if query.from_user.id != int(uid):
      #if query.from_user.id not in OWNER+db.get_admin():
        try:
          return await query.answer(
            "❌ This buttoms is not for you!\nSearch your own book.", show_alert=True
            )
        except:
          return 
    await query.message.delete()        
     
             
                    
            




@Rias.on_callback_query(filters.regex("About"))
async def close_buttom(Rias, query):        
    await query.answer()
    await query.message.edit_text(
        text=f"**Welcome !!."
             "\nHere is a detailed guide to use me."
             "\n\nuse /search <your bookname> ill grab you the direct download links."
             "\n\nyou might had difficulty , in Searching before , we are here to solve it :-) ."
             "\n\nFor further information and guidance contact my developers at my support group.**",
             reply_markup=InlineKeyboardMarkup(
            [
                [
                    InlineKeyboardButton("SUPPORT GROUP", url="https://t.me/BakaForum"),
                ]
            ]
        )
        )




@Rias.on_message(filters.command('broadcast') & filters.user(OWNER))
async def brdcast(Rias, message):
    usr = message.from_user.id
    
    if message.reply_to_message:
        broadcast_msg = message.reply_to_message
        total = 0
        successful = 0
        blocked = 0
        deleted = 0
        unsuccessful = 0
        usrs = get_users()
        usrs = usrs["users"]

        pls_wait = await message.reply("`Broadcasting Message.. This will Take Some Time`")
        for u in usrs:
            u = int(u)
            try:
                await broadcast_msg.copy(u)
                successful += 1
            except FloodWait as e:
                await asyncio.sleep(e.x)
                await broadcast_msg.copy(u)
                successful += 1
            except UserIsBlocked:
                blocked += 1
                await remove_users(u)
            except InputUserDeactivated:
                deleted += 1
                await remove_users(u)
            except:
                #await remove_users(u)
                unsuccessful += 1
                pass
            total += 1
        status = f"""✅<b><u>Broadcast Completed</u>

Total Users: <code>{total}</code>
Successful: <code>{successful}</code>
Blocked Users: <code>{blocked}</code>
Deleted Accounts: <code>{deleted}</code>
Unsuccessful: <code>{unsuccessful}</code></b>"""

        return await pls_wait.edit(status)
    else:
        return await message.reply_text("❌**REPLY TO BROADCAST MESSAGE**")




 
Rias.run()   
    
    
    
    


